module ssrf

go 1.21.8

require (
	github.com/go-ldap/ldap v3.0.3+incompatible
	github.com/jlaffaye/ftp v0.2.0
)

require (
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
)
